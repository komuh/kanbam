<?php

/**
 * Created by PhpStorm.
 * User: Zubb
 * Date: 17/06/15
 * Time: 09:36
 */
class MY_Controller
{

    protected function responseJSON($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
    }

}