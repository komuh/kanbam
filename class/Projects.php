<?php

require_once 'vendor/autoload.php';
require_once "MY_Controller.php";

/**
 * Created by PhpStorm.
 * User: Zubb
 * Date: 17/06/15
 * Time: 09:33
 */
class Projects extends MY_Controller
{
    private $client;

    function __construct($redmine_uri, $api_key)
    {
        $this->client = new Redmine\Client($redmine_uri, $api_key);
    }

    public function getProjects()
    {
        $this->responseJSON($this->client->api('project')->all(array('limit' => 999)));
    }

    public function stories($project_id)
    {
        $this->responseJSON($this->client->api('version')->all($project_id));
    }

    public function loadUsersByProject($project_id)
    {
        $this->responseJSON($this->client->api('membership')->all($project_id));
    }

    public function getTasksProject($id, $offset)
    {
        $this->responseJSON($this->client->api('issue')->all(array(
            'project_id' => $id,
            'limit' => 9999,
            'offset' => $offset
        )));
    }

    public function activities()
    {
        $this->responseJSON($this->client->api('tracker')->all());
    }

    public function loadSpentTimesByProject($project_id)
    {
        $this->responseJSON($this->client->api('time_entry')->all(array(
            'project_id' => $project_id,
            'limit' => 999
        )));
    }

    public function users()
    {
        $this->responseJSON($this->client->api('user')->getCurrentUser());
    }


}