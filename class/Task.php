<?php

require_once 'vendor/autoload.php';
require_once "MY_Controller.php";

/**
 * Created by PhpStorm.
 * User: Zubb
 * Date: 17/06/15
 * Time: 09:33
 */
class Task extends MY_Controller
{
    private $client;

    function __construct($redmine_uri, $api_key)
    {
        $this->client = new Redmine\Client($redmine_uri, $api_key);
    }

    public function add($project_id, $name, $story_id, $activity_id, $hours)
    {
        $this->client->api('issue')->create(array(
            'project_id' => $project_id,
            'subject' => $name,
            'fixed_version_id' => $story_id,
            'tracker_id' => $activity_id,
            'estimated_hours' => $hours
        ));
        $this->responseJSON(array("error" => FALSE));
    }

    public function changeStatus($id, $status, $story)
    {
        $this->client->api('issue')->update($id, array(
            'status' => $status,
            'fixed_version_id' => $story
        ));
        $this->responseJSON(array("error" => FALSE));
    }

    public function update($id, $subject, $estimated_hours, $assigned_to_id)
    {
        $this->client->api('issue')->update($id, array(
            'subject' => $subject,
            'estimated_hours' => $estimated_hours,
            'assigned_to_id' => $assigned_to_id
        ));
        $this->responseJSON(array("error" => FALSE));
    }

    public function remove($id)
    {
        $this->client->api('issue')->remove($id);
        $this->responseJSON(array("error" => FALSE));
    }

    public function addSpentTime($issue_id, $hours, $activity_id)
    {
        $this->client->api('time_entry')->create(array(
            'issue_id' => $issue_id,
            'hours' => $hours,
            'activity_id' => $activity_id
        ));
        $this->responseJSON(array("error" => FALSE));
    }

    public function removeSpentTime($id)
    {
        $this->client->api('time_entry')->remove($id);
        $this->responseJSON(array("error" => FALSE));
    }

    public function loadSpentTimeActivities()
    {
        $this->responseJSON($this->client->api('time_entry_activity')->all());
    }

    public function loadLastsTasks()
    {
        $this->responseJSON($this->client->api('issue')->all(array('limit' => 100)));
    }


}