<?php

require_once 'vendor/autoload.php';
require_once "MY_Controller.php";

/**
 * Created by PhpStorm.
 * User: Zubb
 * Date: 17/06/15
 * Time: 09:33
 */
class Story extends MY_Controller
{
    private $client;

    function __construct($redmine_uri, $api_key)
    {
        $this->client = new Redmine\Client($redmine_uri, $api_key);
    }


    public function add($project_id, $name, $date)
    {
        $this->client->api('version')->create($project_id, array(
            'name' => $name,
            'due_date' => $date
        ));
        $this->responseJSON(array("error" => FALSE));
    }

}