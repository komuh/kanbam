app.controller("AppCtrl", function ($stateParams, $state, $scope, $localStorage, task) {

    var init = function () {
        getCurrentUser();
    };

    var getCurrentUser = function () {
        if ($localStorage.getItem("api_key") && $localStorage.getItem("redmine_uri")) {
            task.postAuth("loadCurrentUser", {}, successUser, error);
            console.log($stateParams);
            if ($stateParams.id) {
                getInfosProject($stateParams.id);
            }
        } else {
            $(".settings").modal();
        }
    };

    var successUser = function (resp) {
        if (resp.user) {
            task.postAuth("loadProjects", {}, successProjects, error);
        }
    };

    var successProjects = function (resp) {
        $scope.projects = resp.projects;
        $scope.$apply();
    };


    $scope.projectSelect = function (id) {
        $state.go("home.app", {
            id: id
        });

        getInfosProject(id);
    };

    var getInfosProject = function (id) {
        task.postAuth("loadStories", {
            project_id: id
        }, successStories, error);
        task.postAuth("loadSpentTimesByProjectId", {
            project_id: id
        }, successSpentTimesProject, error);
    };

    var successStories = function (resp) {
        var ids = [];
        var order = [];
        var orderNew = [];

        for (var key in resp.versions) {
            var obj = resp.versions[key];
            orderNew.push(moment(obj.due_date, "YYYY-MM-DD").unix());
        }

        orderNew.sort();
        for (var i = 0; i < orderNew.length; i++) {
            for (var key in resp.versions) {
                var obj = resp.versions[key];
                var time = moment(obj.due_date, "YYYY-MM-DD").unix();

                if (time == orderNew[i] && !task.contains(ids, obj.id)) {
                    ids.push(obj.id);
                    order.push(obj);
                }
            }
        }

        console.log(order);
    };

    var successSpentTimesProject = function (resp) {
        //console.log(resp);
    };

    var error = function () {

    };

    $scope.$on('$stateChangeSuccess', init);
});
