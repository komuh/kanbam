app.controller("SettingCtrl", function ($scope, task, $localStorage) {

    $scope.formSetting = {};

    $scope.user = function (form) {
        task.post("loadCurrentUser", form, success, error);
    };

    var success = function (resp) {
        // Local Storage
        $localStorage.setItem("api_key", resp.user.api_key);
        $localStorage.setItem("name", resp.user.firstname + " " + resp.user.lastname);
        $localStorage.setItem("redmine_uri", $scope.formSetting.redmine_uri);

        $scope.formSetting = resp.user;
        $(".settings").modal("hide");
    };

    var error = function () {
    };

});
