var app = angular.module("project", ['ui.router']);

app.config(function ($stateProvider, $urlRouterProvider) {
    var patchView = "views/";

    $urlRouterProvider.otherwise("/home/app/");

    $stateProvider
        .state('home', {
            url: "/home",
            templateUrl: patchView + "app.html",
            abstract: true,
            controller: "AppCtrl"
        })
        .state('home.app', {
            url: "/app/:id",
            views: {
                "contentStories": {
                    templateUrl: patchView + "stories.html",
                    controller: "StoriesCtrl"
                },
                "contentSettings": {
                    templateUrl: patchView + "setting.html",
                    controller: "SettingCtrl"
                }
            }
        })
});