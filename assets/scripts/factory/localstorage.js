app.factory("$localStorage", function () {

    var services = {}

    services.setItem = function (key, val) {
        window.localStorage.setItem(key, val);
        if (window.localStorage.getItem(key)) {
            return true;
        }

        return false;
    };

    services.getItem = function (key) {
        return window.localStorage.getItem(key);
    };

    services.removeItem = function (key) {
        window.localStorage.removeItem(key);
        if (window.localStorage.getItem(key)) {
            return false;
        }

        return true;
    };

    return services;
});
