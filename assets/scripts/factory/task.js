app.factory("task", function (constant, $localStorage) {
    var service = {};

    /**
     * Postagem para a api
     * @param {String} url
     * @param {object} data
     * @param {function} success
     * @param {function} error
     * @returns {function}
     */
    service.post = function (url, data, success, error) {
        $.ajax({
            type: "POST",
            url: constant.url + url,
            data: data,
            success: success,
            error: error,
            dataType: "json"
        });
    };

    service.postAuth = function (url, data, success, error) {
        data.api_key = $localStorage.getItem("api_key");
        data.redmine_uri = $localStorage.getItem("redmine_uri");
        $.ajax({
            type: "POST",
            url: constant.url + url,
            data: data,
            success: success,
            error: error,
            dataType: "json"
        });
    };


    service.contains = function (a, obj) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    };

    return service;
});