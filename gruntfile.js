module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        meta: {
            src: {
                assets: {
                    raiz: "assets/",
                    img: "<%= meta.src.assets.raiz %>images/",
                    style: "<%= meta.src.assets.raiz %>styles/",
                    script: "<%= meta.src.assets.raiz %>scripts/",
                    font: "<%= meta.src.assets.raiz %>fonts/"
                },
                view: "views/"
            }
        },
        watch: {
            html: {
                files: [
                    'index.tpl.html', '<%= meta.src.assets.script %>lib/*.js', '<%= meta.src.assets.script %>lib/*.css'
                ],
                tasks: ['clean:indexHTML', 'includeSource:liveReload']
            },
            outros: {
                files: [
                    '<%= meta.src.view %>**/*.html',
                    '<%= meta.src.assets.script %>**/*.js',
                    '<%= meta.src.assets.font %>**',
                    '<%= meta.src.assets.img %>**',
                    '<%= meta.src.assets.style %>*.css'
                ],
                tasks: ['includeSource:liveReload']
            },
            options: {
                livereload: true
            }
        },
        includeSource: {
            options: {
                basePath: '',
                baseUrl: ''
            },
            liveReload: {
                files: {
                    "index.html": "index.tpl.html"
                }
            }
        },
        clean: {
            indexHTML: ["index.html"]
        }
    });

    //Comandos
    grunt.registerTask("itens", ['clean:indexHTML', 'includeSource:liveReload']);
    grunt.registerTask('devel', ['itens', 'watch']);

}
