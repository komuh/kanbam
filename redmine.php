<?php

require_once 'class/Projects.php';
require_once 'class/Task.php';
require_once 'class/Story.php';

$api_key = $_REQUEST["api_key"];
$redmine_uri = $_REQUEST["redmine_uri"];
$action = $_REQUEST["action"];

$project = new Projects($redmine_uri, $api_key);
$task = new Task($redmine_uri, $api_key);
$story = new Story($redmine_uri, $api_key);

switch ($action) {
    // Projects
    case "loadProjects":
        $project->getProjects();
        break;
    case "loadUsersByProjectId":
        $project->loadUsersByProject($_REQUEST["project_id"]);
        break;
    case "loadStories":
        $project->stories($_REQUEST["project_id"]);
        break;
    case "loadTasksByProjectId":
        $project->getTasksProject($_REQUEST["id"], $_REQUEST["offset"]);
        break;
    case "loadSpentTimesByProjectId":
        $project->loadSpentTimesByProject($_REQUEST["project_id"]);
        break;

    // Tasks
    case "loadLastsTasks":
        $task->loadLastsTasks();
        break;
    case "addTask":
        $task->add($_REQUEST["project_id"], $_REQUEST["name"], $_REQUEST["story_id"], $_REQUEST["activity_id"], $_REQUEST["hours"]);
        break;
    case "changeStatusTask":
        $task->changeStatus($_REQUEST["id"], $_REQUEST["status"], $_REQUEST["story"]);
        break;
    case "updateTask":
        $task->update($_REQUEST["id"], $_REQUEST["subject"], $_REQUEST["estimated_hours"], $_REQUEST["assigned_to_id"]);
        break;
    case "removeTask":
        $task->remove($_REQUEST["id"]);
        break;
    case "removeSpentTime":
        $task->removeSpentTime($_REQUEST["id"]);
        break;
    case "loadSpentTimeActivities":
        $task->loadSpentTimeActivities();
        break;
    case "addSpentTime":
        $task->addSpentTime($_REQUEST["issue_id"], $_REQUEST["hours"], $_REQUEST["activity_id"]);
        break;

    // Story
    case "addStory":
        $story->add($_REQUEST["project_id"], $_REQUEST["name"], $_REQUEST["date"]);
        break;


    // Others
    case "loadActivities":
        $project->activities();
        break;
    case "loadCurrentUser":
        $project->users();
        break;
}
